#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdbool.h>
#include <sys/stat.h>

int threadCounter;
char *downloadLink[2] = {"https://docs.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1",
                         "https://docs.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt"};
char *zipFileNames[2];
char *dirName[2] = {"music", "quote"};
pthread_t tid[2];

void rmDir(char dirName[])
{
    if (fork() == 0)
    {
        char *arguments[] = {"rm", "-r", dirName, NULL};
        execv("/bin/rm", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}

void rmFile(char fileName[])
{
    if (fork() == 0)
    {
        char *arguments[] = {"rm", "-r", fileName, NULL};
        execv("/usr/bin/rm", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}

void makeDirectory(char dirName[])
{
    if (fork() == 0)
    {
        char *arguments[] = {"mkdir", dirName, NULL};
        execv("/bin/mkdir", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}

void *downloadFiles(void *count)
{
    (char *)count;

    int intCount = atoi(count);
    char fileName[150];

    sprintf(fileName, "zipFileNo%d.zip", intCount);
    printf("This is filename: %s\n", fileName);
    strcpy(zipFileNames[intCount], fileName);
    if (fork() == 0)
    {
        char *arguments[] = {"wget", "--no-check-certificate", downloadLink[intCount], "-O", fileName, NULL};
        execv("/bin/wget", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
}

void *unzip(void *count)
{
    (char *)count;
    int intCount = atoi(count);
    char pathToFolder[50];
    sprintf(pathToFolder, "./%s/", dirName[intCount]);

    if (fork() == 0)
    {
        char *arguments[] = {"unzip", "-q", zipFileNames[intCount], "-d", pathToFolder, NULL};
        execv("/bin/unzip", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
}

size_t baseSixtyFourLength(const char *in)
{
    size_t len;
    size_t ret;
    size_t i;

    if (in == NULL)
        return 0;

    len = strlen(in);
    ret = len / 4 * 3;

    for (i = len; i-- > 0;)
    {
        if (in[i] == '=')
        {
            ret--;
        }
        else
        {
            break;
        }
    }

    return ret;
}

bool baseSixtyFourCharCheck(char c)
{
    if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c == '+' || c == '/' || c == '='))
        return true;
    return false;
}

char *decodeBaseSixtyFour(char code[], size_t codeLen)
{
    char *answerString = (char *)malloc(sizeof(char) * 100);
    int k = 0;
    int num = 0;
    int bitsCount = 0;

    for (size_t i = 0; i < codeLen; i += 4)
    {
        bitsCount = 0;
        num = 0;
        for (int j = 0; j < 4; j++)
        {
            if (code[i + j] != '=')
            {
                num = num << 6;
                bitsCount += 6;
            }
            if (code[i + j] >= 'A' && code[i + j] <= 'Z')
                num = num | (code[i + j] - 'A');

            else if (code[i + j] >= 'a' && code[i + j] <= 'z')
                num = num | (code[i + j] - 'a' + 26);

            else if (code[i + j] >= '0' && code[i + j] <= '9')
                num = num | (code[i + j] - '0' + 52);

            else if (code[i + j] == '+')
                num = num | 62;

            else if (code[i + j] == '/')
                num = num | 63;

            else
            {
                num = num >> 2;
                bitsCount -= 2;
            }
        }

        while (bitsCount != 0)
        {
            bitsCount -= 8;
            answerString[k++] = (num >> bitsCount) & 255;
        }
    }
    answerString[k] = '\n';
    answerString[k + 1] = '\0';

    return answerString;
}

void *readAndDecode(void *count)
{

    (char *)count;
    int intCount = atoi(count);
    char dir[300], outputFiledir[300], buffer[1000];

    sprintf(dir, "./%s/", dirName[intCount]);
    sprintf(outputFiledir, "%s%s.txt", dir, dirName[intCount]);

    char *fileNames[9];
    for (int i = 0; i < 9; i++)
    {
        fileNames[i] = (char *)malloc(sizeof(char) * 100);

        if (intCount == 0)
        {
            sprintf(fileNames[i], "music/m%d.txt", i + 1);
        } // Music
        else
        {
            sprintf(fileNames[i], "quote/q%d.txt", i + 1);
        } // quote
    }

    FILE *nonDecodedFiles[9];
    FILE *outputFile = fopen(outputFiledir, "w+");

    if (outputFile == NULL)
    {
        fprintf(stderr, "OutputFile ptr is null\n");
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < 9; i++)
    {
        nonDecodedFiles[i] = fopen(fileNames[i], "r");

        if (nonDecodedFiles[i] == NULL)
        {
            fprintf(stderr, "nonDecodedFiles ptr is null\n");
            exit(EXIT_FAILURE);
        }

        fgets(buffer, 200, nonDecodedFiles[i]);

        char *answerChar = decodeBaseSixtyFour(buffer, strlen(buffer));

        fprintf(outputFile, "%s", answerChar);

        strcpy(buffer, "");
        free(answerChar);
        fclose(nonDecodedFiles[i]);
    }

    fclose(outputFile);
}

void mvFile(char filePath[], char destPath[])
{
    if (fork() == 0)
    {
        char *arguments[] = {"mv", filePath, destPath, NULL};
        execv("/bin/mv", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}

void cpFile(char filePath[], char destPath[])
{
    if (fork() == 0)
    {
        char *arguments[] = {"cp", filePath, destPath, NULL};
        execv("/bin/cp", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}

void hasilZip()
{
    if (fork() == 0)
    {
        char *arguments[] = {"zip", "-P", "mihinomenestfico", "-r", "hasil.zip", "hasil", "-m", NULL};
        execv("/bin/zip", arguments);
        exit(EXIT_SUCCESS);
    }

    int status;
    while (wait(&status) >= 0)
        ;

    return;
}

void unzipNormPass(char zipName[], char pass[])
{
    if (fork() == 0)
    {
        // Unzip Characters.zip
        char *arguments[] = {"unzip", "-P", "mihinomenestfico", zipName, NULL};
        execv("/bin/unzip", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}

void createFile(char fileName[], char contents[])
{
    FILE *fptr = fopen(fileName, "w+");
    fprintf(fptr, "%s", contents);
    fclose(fptr);
}

void *unzipOrCreateFile(void *count)
{
    pthread_t curr = pthread_self();

    if (curr == tid[0])
    {
        printf("Nyampe sini kgk\n");
        unzipNormPass("hasil.zip", "mihinomenestfico");
        // unzip
    }
    else
    {
        createFile("./hasil/no.txt", "No\n");
        // createFile
    }
}

int main(int argc, char const *argv[])
{
    threadCounter = 0;
    char *currThread[2] = {"0", "1"};
    for (int i = 0; i < 2; i++)
    {
        zipFileNames[i] = (char *)malloc(sizeof(char) * 100);
    }

    // Download Files, zipNum0 is music.zip, zipNum1 is quote.zip
    for (int i = 0; i < 2; i++)
    {
        pthread_create(&(tid[i]), NULL, &downloadFiles, (void *)currThread[i]);
        threadCounter++;
    }

    for (int i = 0; i < 2; i++)
    {
        pthread_join(tid[i], NULL);
        threadCounter--;
    }

    if (threadCounter != 0)
        exit(EXIT_FAILURE);

    // Create directory
    makeDirectory("music");
    makeDirectory("quote");
    makeDirectory("hasil");

    // Unzip Files
    for (int i = 0; i < 2; i++)
    {
        pthread_create(&(tid[i]), NULL, &unzip, (void *)currThread[i]);
        threadCounter++;
    }

    for (int i = 0; i < 2; i++)
    {
        pthread_join(tid[i], NULL);
        threadCounter--;
    }

    if (threadCounter != 0)
        exit(EXIT_FAILURE);

    rmFile(zipFileNames[0]);
    rmFile(zipFileNames[1]);

    // Write to music.txt and quote.txt

    for (int i = 0; i < 2; i++)
    {
        pthread_create(&(tid[i]), NULL, &readAndDecode, (void *)currThread[i]);
        threadCounter++;
    }

    for (int i = 0; i < 2; i++)
    {
        pthread_join(tid[i], NULL);
        threadCounter--;
    }

    if (threadCounter != 0)
        exit(EXIT_FAILURE);

    sleep(1);

    cpFile("./music/music.txt", "hasil/music.txt");
    cpFile("./quote/quote.txt", "hasil/quote.txt");

    sleep(1);
    hasilZip();

    makeDirectory("hasil");

    for (int i = 0; i < 2; i++)
    {
        pthread_create(&(tid[i]), NULL, &unzipOrCreateFile, NULL);
        threadCounter++;
    }

    for (int i = 0; i < 2; i++)
    {
        pthread_join(tid[i], NULL);
        threadCounter--;
    }

    if (threadCounter != 0)
        exit(EXIT_FAILURE);

    rmFile("hasil.zip");
    sleep(1);
    hasilZip();

    // // TODO masih ada fuse hidden
    // // rmdir("hasil");
    // // rmDir("quote");
    // // rmDir("music");

    return 0;
}
