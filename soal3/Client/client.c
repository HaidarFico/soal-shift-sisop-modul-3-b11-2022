#include <stdio.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdbool.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <libgen.h>
#include <arpa/inet.h>
#include <wait.h>
#define PORT 8080

char zipPathGlobal[100] = "/home/haidarwsl/shift3/";
char destPathGlobal[100] = "/home/haidarwsl/VSCodeTest/soal-shift-sisop-modul-3-b11-2022/soal3/Client/hartakarun.zip";

int sock;

void zip(char zipPath[], char destPath[])
{
    if (fork() == 0)
    {
        // Unzip Characters.zip
        char *arguments[] = {"zip", "-r", destPath, zipPath, NULL};
        execv("/bin/zip", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}

void fileSend(int sock, char fileName[])
{
    char buffer[1024] = {0};
    FILE *fptr = fopen(fileName, "rb");

    if(fptr == NULL)
    {
        perror("Error opening file\n");
        exit(EXIT_FAILURE);
    }

    bzero(buffer, 1024);

    size_t fileSize;
    while (true)
    {
        fileSize = fread(buffer, 1, 1024, fptr);
        printf("This is buffer: %s\n", buffer);

        if(fileSize > 0)
        {
            send(sock, buffer, 1024, 0);
        }

        else
        {
            break;
        }

        bzero(buffer, 1024);
    }
    
    printf("Selesai\n");
    fclose(fptr);
}

int main(int argc, char const *argv[])
{
    // Create socket and connect to server
    struct sockaddr_in address;
    int valread;
    sock = 0;
    struct sockaddr_in serv_addr;
    char *hello = "Hello from client";
    char buffer[1024] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) == -1)
    {
        printf("\nConnection Failed \n");
        return -1;
    }

    printf("Connected\n");
    char buffTemp[] = "Hello from client!";
    send(sock, buffTemp, strlen(buffTemp), 0);
    
    char input[50];
    char compare[] = "send hartakarun.zip\n";
    while (strcmp(input, compare) != 0)
    {
        printf("Please enter a command:\n");
        fgets(input, 50, stdin);
    }

    zip(zipPathGlobal, destPathGlobal);
    sleep(1);
    fileSend(sock, destPathGlobal);

    close(sock);

    return 0;
}
