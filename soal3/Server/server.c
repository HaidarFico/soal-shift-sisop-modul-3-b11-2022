#include <stdio.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdbool.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <libgen.h>
#include <arpa/inet.h>
#define PORT 8080

char destPathGlobal[100] = "./hartakarun.zip";


void fileDownload(int socket)
{
    char buffer[1024] = {0};

    FILE *fptr = fopen(destPathGlobal, "ab");

    if(fptr == NULL)
    {
        perror("Error creating FILE pointer\n");
        exit(EXIT_FAILURE);
    }

    bzero(buffer, 1024);
    size_t fileSize = 0;
    while((fileSize = recv(socket, buffer, 1024, 0)) > 0)
    {
        if(fileSize == 0)
        {
            break;
        }
        size_t writeSize = fwrite(buffer, sizeof(char), fileSize, fptr);
        if(writeSize < fileSize)
        {
            perror("Error transferring files\n");
            exit(EXIT_FAILURE);
        }

        bzero(buffer, 1024);

    }

    fclose(fptr);

    printf("Berhasil\n");

}

int main(int argc, char const *argv[])
{
    // Create socket and listen to client
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *hello = "Hello from server";
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    printf("Berhasil connect\n");
    char tempBuff[1024];
    valread = read(new_socket, tempBuff, 1024);
    printf("%s\n",tempBuff);

    fileDownload(new_socket);

    close(new_socket);

    return 0;
}
