#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>

// Change these variables accordingly
char dirPathGlobal[100] = "/home/haidarwsl/shift3";
char zipName[100] = "hartakarun.zip";
char dirPathWorking[100] = "/home/haidarwsl/shift3/hartakarun";

pthread_t tid[100];
int threadCount;

void unzip(char zipName[], char dirPath[])
{
    if (fork() == 0)
    {
        // Unzip Characters.zip
        char *arguments[] = {"unzip", "-q", zipName, "-d", dirPath, NULL};
        execv("/bin/unzip", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}

void *moveFile(void *filePath)
{
    char *filePathInFunc = (char *)filePath;
    // printf("DEBUG:: This is filePath in moveFile func: %s\n", filePathInFunc);
    struct stat *sb;

    // char fileName[300] = strrchr(filePathInFunc, '/');
    char *fileName = strrchr(filePathInFunc, '/');
    fileName++;

    // printf("DEBUG:: This is fileName: %s\n", fileName);

    char categoryName[100], newDir[1000], fullNewDir[1000];
    char *extensionName;

    // printf("DEBUG:: filename[0] %c\n", fileName[0]);
    if (fileName[0] == '.')
    {
        // Hidden files
        sprintf(categoryName, "Hidden");
        // printf("DEBUG:: File dengan hidden files jalan\n");
    }

    else if ((extensionName = strstr(filePathInFunc, ".")) != NULL)
    {
        // File dengan ekstensi biasa
        extensionName++;
        char extensionTemp[100];
        strcpy(extensionTemp, extensionName);
        for(int i = 0; i < strlen(extensionTemp); i++)
        {
            extensionTemp[i] = tolower(extensionTemp[i]);
        }
        sprintf(categoryName, "%s", extensionTemp);
        // printf("DEBUG:: This is extensionName %s\n", extensionName);
    }

    else
    {
        // File dengan ekstensi yang tidak diketahui
        sprintf(categoryName, "Unknown");
        // printf("DEBUG:: File dengan ekstensi yang tidak diketahui ran\n");
    }

    // printf("DEBUG:: This is categoryName: %s\n", categoryName);

    // Membuat direktori baru
    sprintf(newDir, "%s/%s", dirPathWorking, categoryName);
    if (stat(newDir, sb) == 0 && S_ISDIR(sb->st_mode))
    {
        // Directory sudah ada
        // printf("dir udah ada\n");
    }
    else
    {
        // Directory belum ada
        mkdir(newDir, 0777);
    }

    // Memindahkan file
    sprintf(fullNewDir, "%s/%s", newDir, fileName);
    rename(filePathInFunc, fullNewDir);
}

void sortFun(char dirPath[])
{
    struct dirent *direntPoint;
    struct stat *fileStat;
    DIR *dirList = opendir(dirPath);
    char filePathTemp[100];
    threadCount = 0;

    if (!dirList)
    {
        // dirList empty error
        perror("dirList empty\n");
        exit(EXIT_FAILURE);
    }

    // Reads the folder
    while ((direntPoint = readdir(dirList)) != NULL)
    {
        if (strcmp("..", direntPoint->d_name) != 0 || strcmp(".", direntPoint->d_name) != 0)
        {
            // The ".." and "." will be filtered in this if statement
            sprintf(filePathTemp, "%s/%s", dirPath, direntPoint->d_name);
            // printf("DEBUG:: THIS IS filePathTemp: %s\n", filePathTemp);
            // if (stat(filePathTemp, fileStat) == 0 && S_ISREG(fileStat->st_mode))
            if ( access( filePathTemp, F_OK) == 0)
            {
                // Directory ada
                // moveFile(filePathTemp);
                pthread_t thread;
                pthread_create(&thread, NULL, &moveFile, (void *)filePathTemp);
                pthread_join(thread, NULL);
                // threadCount++;
            }
        }
    }

    // for (; threadCount > 0; threadCount--)
    // {
    //     // Joins all the threads
    //     pthread_join(tid[threadCount], NULL);
    // }

    closedir(dirList);
}

int main(int argc, char const *argv[])
{
    // Make the folder
    // char *userName = getlogin(); getLogin gabisa di WSL

    // printf("DEBUG:: this is dirpath %s\n", dirPathGlobal);

    mkdir(dirPathGlobal, 0777);

    // Unzip the files
    unzip(zipName, dirPathGlobal);

    // Start the sort
    sortFun(dirPathWorking);

    return 0;
}
