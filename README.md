Soal 1

a. Untuk bagian pertama menggunakan function downloadFiles dan unzip. Kedua function
tersebut akan menggunakan satu thread yang dibuat dalam main function. Function mereka
adalah sebagai berikut:
```c
void *downloadFiles(void *count)
{
    (char *)count;

    int intCount = atoi(count);
    char fileName[150];

    sprintf(fileName, "zipFileNo%d.zip", intCount);
    printf("This is filename: %s\n", fileName);
    strcpy(zipFileNames[intCount], fileName);
    if (fork() == 0)
    {
        char *arguments[] = {"wget", "--no-check-certificate", downloadLink[intCount], "-O", fileName, NULL};
        execv("/bin/wget", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
}
```

Untuk unzip adalah:
```c
void *unzip(void *count)
{
    (char *)count;
    int intCount = atoi(count);
    char pathToFolder[50];
    sprintf(pathToFolder, "./%s/", dirName[intCount]);

    if (fork() == 0)
    {
        char *arguments[] = {"unzip", "-q", zipFileNames[intCount], "-d", pathToFolder, NULL};
        execv("/bin/unzip", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
}
```
Kedua function ini menggunakan execv untuk berjalan. Karena execv akan memberhentikan
process C, maka dibutuhkan fork untuk membuat process baru. Argumen count dalam 
function ini digunakan untuk membagi tugas antara thread pertama dan thread kedua.

Pembuatan thread terdapat pada main function.

b. Untuk mendecode base64 menggunakan function decodeBaseSixtyFour yang akan menerima
argumen berupa string base64 yang ingin di dekode dan panjang dari string tersebut. 
Function akan dimulai dengan mengalokasi memori untuk string. Lalu, akan mengambil
data dari string per 4 karakter lalu mendapatkan karakter string tersebut dalam ASCII
table. Setelah itu, karakter tersebut akan dimasukkan ke dalam string hasil. Terakhir
akan dimasukkan escape sequence newline dan EOF dan direturn.
Kodenya adalah sebagai berikut:
```c
char *decodeBaseSixtyFour(char code[], size_t codeLen)
{
    char *answerString = (char *)malloc(sizeof(char) * 100);
    int k = 0;
    int num = 0;
    int bitsCount = 0;

    for (size_t i = 0; i < codeLen; i += 4)
    {
        bitsCount = 0;
        num = 0;
        for (int j = 0; j < 4; j++)
        {
            if (code[i + j] != '=')
            {
                num = num << 6;
                bitsCount += 6;
            }
            if (code[i + j] >= 'A' && code[i + j] <= 'Z')
                num = num | (code[i + j] - 'A');

            else if (code[i + j] >= 'a' && code[i + j] <= 'z')
                num = num | (code[i + j] - 'a' + 26);

            else if (code[i + j] >= '0' && code[i + j] <= '9')
                num = num | (code[i + j] - '0' + 52);

            else if (code[i + j] == '+')
                num = num | 62;

            else if (code[i + j] == '/')
                num = num | 63;

            else
            {
                num = num >> 2;
                bitsCount -= 2;
            }
        }

        while (bitsCount != 0)
        {
            bitsCount -= 8;
            answerString[k++] = (num >> bitsCount) & 255;
        }
    }
    answerString[k] = '\n';
    answerString[k + 1] = '\0';

    return answerString;
}
```
Untuk memberi argumen string base64 nya dibutuhkan untuk membaca file-file yang dari
hasil unzip. Function ini akan memanggil function decodeBaseSixtyFour untuk mendapatkan
string hasilnya dan menulisnya ke .txt file. Array of FILE akan digunakan untuk membaca
file txt tersebut. Kodenya adalah sebagai berikut:
```c
void *readAndDecode(void *count)
{

    (char *)count;
    int intCount = atoi(count);
    char dir[300], outputFiledir[300], buffer[1000];

    sprintf(dir, "./%s/", dirName[intCount]);
    sprintf(outputFiledir, "%s%s.txt", dir, dirName[intCount]);

    char *fileNames[9];
    for (int i = 0; i < 9; i++)
    {
        fileNames[i] = (char *)malloc(sizeof(char) * 100);

        if (intCount == 0)
        {
            sprintf(fileNames[i], "music/m%d.txt", i + 1);
        } // Music
        else
        {
            sprintf(fileNames[i], "quote/q%d.txt", i + 1);
        } // quote
    }

    FILE *nonDecodedFiles[9];
    FILE *outputFile = fopen(outputFiledir, "w+");

    if (outputFile == NULL)
    {
        fprintf(stderr, "OutputFile ptr is null\n");
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < 9; i++)
    {
        nonDecodedFiles[i] = fopen(fileNames[i], "r");

        if (nonDecodedFiles[i] == NULL)
        {
            fprintf(stderr, "nonDecodedFiles ptr is null\n");
            exit(EXIT_FAILURE);
        }

        fgets(buffer, 200, nonDecodedFiles[i]);

        char *answerChar = decodeBaseSixtyFour(buffer, strlen(buffer));

        fprintf(outputFile, "%s", answerChar);

        strcpy(buffer, "");
        free(answerChar);
        fclose(nonDecodedFiles[i]);
    }

    fclose(outputFile);
}
```
c. Pemindahan file akan dilakukan dengan function mvFile setelah membuat direktori hasil
dengan makeDirectory. Function-function ini merupakan wrapper untuk 
execv mv dan mkdir. Kodenya adalah sebagai:
```c
void mvFile(char filePath[], char destPath[])
{
    if (fork() == 0)
    {
        char *arguments[] = {"mv", filePath, destPath, NULL};
        execv("/bin/mv", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}
```
```c
void makeDirectory(char dirName[])
{
    if (fork() == 0)
    {
        char *arguments[] = {"mkdir", dirName, NULL};
        execv("/bin/mkdir", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}
```
d. Zip file juga menggunakan wrapper untuk execv zip. Password yang digunakan berupa
minihomenestfico dengan menggunakan attribut -P di zip. Function nya adalah sebagai 
berikut:
```c
void hasilZip()
{
    if (fork() == 0)
    {
        char *arguments[] = {"zip", "-P", "mihinomenestfico", "-r", "hasil.zip", "hasil", "-m", NULL};
        execv("/bin/zip", arguments);
        exit(EXIT_SUCCESS);
    }

    int status;
    while (wait(&status) >= 0)
        ;

    return;
}
```
e. Untuk mengunzip karena ada password maka menambah -P dalam atribut unzip, sehingga menggunakan function yang sedikit berbeda. Setelah itu, akan ada function createFile
yang akan melakukan fopen pada file baru yang akan diisi NO. Pengerjaan kedua function
ini akan menggunakan thread yang diatur dalam unzipOrCreateFile. Kode adalah
sebagai berikut:
```c
void *unzipOrCreateFile(void *count)
{
    pthread_t curr = pthread_self();

    if (curr == tid[0])
    {
        printf("Nyampe sini kgk\n");
        unzipNormPass("hasil.zip", "mihinomenestfico");
        // unzip
    }
    else
    {
        createFile("./hasil/no.txt", "No\n");
        // createFile
    }
}
```
```c
void createFile(char fileName[], char contents[])
{
    FILE *fptr = fopen(fileName, "w+");
    fprintf(fptr, "%s", contents);
    fclose(fptr);
}
```
```c
void unzipNormPass(char zipName[], char pass[])
{
    if (fork() == 0)
    {
        // Unzip Characters.zip
        char *arguments[] = {"unzip", "-P", "mihinomenestfico", zipName, NULL};
        execv("/bin/unzip", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}
```

Soal 2

a. Saat program dijalankan akan diminta untuk memilih antara register atau login. Jika register, maka
username dan password akan dimasukkan ke dalam users.txt. Jika login, maka program akan membaca users.txt
dan mencocokkan dengan data base yang ada.
Client
```c
if (pil==1){
        send(sock, regist, strlen(regist), 0);
        printf("Mohon masukkan baru ID dan sebuah password.\n");
    }
    else if (pil==2){
        send(sock, login, strlen(login), 0);
        printf("Mohon masukkan ID dan password yang sudah ter-register.\n");
    }
    valread = read(sock , buffer, 1024);

    if (strcmp(buffer,"success")==0){
        memset(buffer,0,sizeof(buffer));

        printf("Masukkan ID: ");
        scanf("%s",id);
        send(sock, id, strlen(id), 0);

        printf("Masukkan password: ");
        scanf("%s",pass);
        send(sock, pass, strlen(pass), 0);
        printf("\n");
```
Server
```c
// Register
    if (strcmp(buffer,"1")==0){
        send(new_socket, success, strlen(success), 0);
        memset(buffer, 0, sizeof(buffer));

        valread = read(new_socket, buffer, 1024);
        strcpy(idpass,buffer);
        strcat(idpass,":");

        FILE *fp = fopen ("akun.txt","a+");
        while (fscanf(fp,"%s", string) == 1){        
            if(strstr(string,idpass)!=0) {
                printf("Akun sudah ada.\n");
                ret = 1;
            }
        }

        valread = read(new_socket, buffer, 1024);
        strcat(idpass,buffer);
        
        if (!isStrong(buffer)){
            printf("Password tidak kuat.\n");
            ret = 1;
        }

        if (ret){
            send(new_socket, gagal, strlen(gagal), 0);
        } else {
            strcat(content, idpass);
            strcat(content,"\n");
            if(fp){
                fputs(content,fp);
            }
            fclose (fp);

            send(new_socket, registered, strlen(registered), 0);
        }
    } 
    
    // Login
    else if (strcmp(buffer,"2")==0){
        send(new_socket, success, strlen(success), 0);
        memset(buffer, 0, sizeof(buffer));

        long length;
        FILE *f = fopen ("akun.txt", "rb");

        if (f){
            valread = read(new_socket, buffer, 1024);
            strcpy(id,buffer);
            strcpy(idpass,buffer);
            strcat(idpass,":");
            valread = read(new_socket, buffer, 1024);
            strcpy(pass,buffer);
            strcat(idpass,buffer);

            while (fscanf(f,"%s", string) == 1){        
                if(strstr(string, idpass)!=0) {
                    ret = 1;
                }
            }
```

b. Saat server dijalankan, problems.tsv otomatis akan dibuat untuk menampung problems.
Server
```c
    FILE * tsv;
    tsv = fopen ("problems.tsv","a+");
```

c. Saat command 'add' dijalankan, akan diminta judul problem. Jika judul problem sudah ada, maka program dihentikan.
Jika belum, maka akan lanjut meminta path file description.txt, input.txt, dan output.txt. Setelah di-add, semua file akan
disimpan pada sebuah folder dengan nama sesuai judulnya dan nama problem beserta author akan dimasukkan dalam problems.tsv.
Client
```c
if(strcmp(cmd,"add")==0){
    send(sock, add, strlen(add), 0 );

    printf("Add Problem\n");
    printf("Judul Problem: ");
    scanf(" %s", judulProblem);
    send(sock, judulProblem, strlen(judulProblem), 0);
    memset(response, 0, 2048);
    recv(sock, response, 2048, 0);
    if(!strcmp(response, "able")){
        printf("Filepath description.txt: ");
        scanf(" %s", descFile);
        send(sock, descFile, strlen(descFile), 0);
        send_file(descFile, sock);
        printf("Filepath input.txt: ");
        scanf(" %s", inputFile);
        send(sock, inputFile, strlen(inputFile), 0);
        send_file(inputFile, sock);
        printf("Filepath output.txt: ");
        scanf(" %s", outputFile);
        send(sock, outputFile, strlen(outputFile), 0);
        send_file(outputFile, sock);
        memset(response, 0, 2048);
        recv(sock, response, 2048, 0);
        printf("%s", response);
    }
    if(!strcmp(response, "unable")){
        printf("Problem dengan judul tersebut sudah ada, gunakan judul lain\n");
    }
}
```
Server
```c
if (strcmp(buffer,"add")==0){
    FILE *fp;
    char judulProblem[64] = {0};
    char descFile[64] = {0};
    char inputFile[64] = {0};
    char outputFile[64] = {0};
    char tmp[512];
    char response[512];

    fp = fopen("problems.tsv", "a+");
    recv(new_socket, judulProblem, 64, 0);
    printf("judul problem: %s\n", judulProblem);
    if(isUniqueProblem(fp, judulProblem)){
        strcpy(response, "able");
        send(new_socket, response, strlen(response), 0);
        mkdir(judulProblem, 0777);
        fprintf(fp, "%s  \t %s\n", judulProblem, id);
        recv(new_socket, descFile, 64, 0);
        strcpy(tmp, judulProblem);
        strcat(tmp, "/");
        strcat(tmp, descFile);
        printf("desc: %s\n", tmp);
        write_file(new_socket, tmp);
        printf("Data written to file\n");
        recv(new_socket, inputFile, 64, 0);
        strcpy(tmp, judulProblem);
        strcat(tmp, "/");
        strcat(tmp, inputFile);
        printf("input: %s\n", tmp);
        write_file(new_socket, tmp);
        printf("Data written to file\n");
        recv(new_socket, outputFile, 64, 0);
        strcpy(tmp, judulProblem);
        strcat(tmp, "/");
        strcat(tmp, outputFile);
        printf("output: %s\n", tmp);
        write_file(new_socket, tmp);
        printf("Data written to file\n");
        strcpy(response, "Problem berhasil ditambahkan\n");
        send(new_socket, response, strlen(response), 0);
    } else{
        strcpy(response, "unable");
        send(new_socket, response, strlen(response), 0);
    }
    fclose(fp);
}
```

d. Saat command 'see' dijalankan, Client akan mendapatkan isi problems.tsv dari Server. Lalu string tersebut diolah
sehingga ' \t' akan diubah menjadi 'by'. 
Client
```c
// See
else if (strcmp(cmd,"see")==0){
    send(sock , see , strlen(see) , 0 );
    memset(buffer,0,sizeof(buffer));
    valread = read(sock, buffer, 1024);
    strcat(isi,buffer);

    int i;
    while(isi[i]!='\0'){
        if(isi[i]=='\t'){
            isi[i-1]='b';
            isi[i]='y';
        }
        i++;
    }

    printf("\n%s",isi);
}
```
Server
```c
// See
else if (strcmp(buffer,"see")==0){
    char *buf = 0;
    char content[1000]={0};
    long length;
    FILE * f = fopen ("problems.tsv", "rb");

    if (f){
        fseek (f, 0, SEEK_END);
        length = ftell (f);
        fseek (f, 0, SEEK_SET);
        buf = malloc (length);
        if (buf){
            fread (buf, 1, length, f);
        }
        fclose (f);
    }
    if (buf){
        strcpy(content,buf);
        send(new_socket , content , strlen(content) , 0 );
    }
}
```

e. Saat menggunakan command 'download', seharusnya program mengunduh description.txt dan input.txt ke folder dengan
nama judul problem-nya.
Client
```c
// Download
else if (strcmp(cmd,"download")==0){
    char tmp[1024];

    send(sock, download, strlen(download), 0);
    sleep(1);
    strcpy(tmp, judul);
    send(sock, tmp, strlen(tmp), 0);
    mkdir(judul, 0777);
    strcpy(tmp, judul);
    strcat(tmp, "/description.txt");
    write_file(sock, tmp);
    strcpy(tmp, judul);
    strcat(tmp, "/input.txt");
    write_file(sock, tmp);
    printf("Problem downloaded\n");
}
```
Server
```c
// Download
 else if (strcmp(buffer,"down")==0){
    char tmp[1024];
    valread = read(new_socket, buffer, 1024);
    strcpy(tmp, buffer);
    strcat(tmp, "/");
    strcat(tmp, "description.txt");
    send_file(tmp, new_socket);
    strcpy(tmp, buffer);
    strcat(tmp, "/");
    strcat(tmp, "input.txt");
    send_file(tmp, new_socket);
}
```

f. Saat menggunakan command 'submit', seharusnya program mengambil output.txt dari Client dan membandingkan dengan
output.txt yang benar.
Client
```c
// Submit
else if (strcmp(cmd,"submit")==0){
    FILE *answer, *key;
    char tmp[1024];
    answer = fopen("answer.txt", "r");
    key = fopen("key.txt", "r");
    if(isSame(answer, key)){
        printf("AC\n");
    } else{
        printf("WA\n");
    }
    fclose(answer);
    fclose(key);
    remove("tmp.txt");
    printf("Done\n");
}
```
Server
```c
// Submit
else if (strcmp(buffer,"submit")==0){
    FILE *answer, *key;
    char buffer[1024] = {0};
    char tmp[1024];
    valread = read(new_socket, buffer, 1024);
    strcpy(tmp, buffer);
    strcat(tmp, "/output.txt");
    write_file(new_socket, "tmp.txt");
    answer = fopen("tmp.txt", "r");
    key = fopen(tmp, "r");
    if(isSame(answer, key)){
        strcpy(tmp, "AC\n");
    } else{
        strcpy(tmp, "WA\n");
    }
    send(new_socket, tmp, strlen(tmp), 0);
    fclose(answer);
    fclose(key);
    remove("tmp.txt");
    printf("Done\n");
}
```

g. Disini Server seharusnya dibuat untuk dapat menangani dua atau lebih Client yang ingin terhubung dengan Server.
Namun saya sendiri belum mengimplementasi hal ini ke code saya.

Soal 3

a. Pembuatan direktori akan menggunakan function mkdir yang terdapat dalam C.
Permission yang digunakan akan berupa 0777 yang berarti read, write, dan execute
untuk owner, group, dan yang lain. Selanjutnya akan melakukan unzip dengan menggunakan
function unzip yang sebagai berikut:
```c
void unzip(char zipName[], char dirPath[])
{
    if (fork() == 0)
    {
        // Unzip Characters.zip
        char *arguments[] = {"unzip", "-q", zipName, "-d", dirPath, NULL};
        execv("/bin/unzip", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}
```
Setelah dilakukan unzip, akan memanggil function sortFun yang mengurusi sortiran dari
file-file hasil unzip. Function ini akan menggunakan DIR dan dirent untuk mengetahui
metadata dari file-file yang akan dipindahkan. Setelah DIR telah terinisialisasi,
DIR tersebut akan dibaca dan file-file hasilnya akan dimasukkan ke dalam function moveFile. 
```c
void sortFun(char dirPath[])
{
    struct dirent *direntPoint;
    struct stat *fileStat;
    DIR *dirList = opendir(dirPath);
    char filePathTemp[100];
    threadCount = 0;

    if (!dirList)
    {
        // dirList empty error
        perror("dirList empty\n");
        exit(EXIT_FAILURE);
    }

    // Reads the folder
    while ((direntPoint = readdir(dirList)) != NULL)
    {
        if (strcmp("..", direntPoint->d_name) != 0 || strcmp(".", direntPoint->d_name) != 0)
        {
            // The ".." and "." will be filtered in this if statement
            sprintf(filePathTemp, "%s/%s", dirPath, direntPoint->d_name);
            // printf("DEBUG:: THIS IS filePathTemp: %s\n", filePathTemp);
            // if (stat(filePathTemp, fileStat) == 0 && S_ISREG(fileStat->st_mode))
            if ( access( filePathTemp, F_OK) == 0)
            {
                // Directory ada
                // moveFile(filePathTemp);
                pthread_t thread;
                pthread_create(&thread, NULL, &moveFile, (void *)filePathTemp);
                pthread_join(thread, NULL);
                // threadCount++;
            }
        }
    }

    // for (; threadCount > 0; threadCount--)
    // {
    //     // Joins all the threads
    //     pthread_join(tid[threadCount], NULL);
    // }

    closedir(dirList);
}
```
b. c.
Function moveFile berhubungan dengan pemindahan file ke folder-folder sortir
tersebut. Function ini menerima argumen file path dari file tersebut. Jika file
tersebut dimulai dengan . maka akan dipindahkan ke dalam folder Hidden. Jika
tidak ada . dalam file tersebut, maka akan dimasukkan ke dalam folder Unknown. Yang
terakhir, akan dimasukkan ke dalam folder sesuai file typenya. File typenya akan
dibuat menjadi lowercase sesuai ketentuan. Akan dibuat folder baru jika folder dengan
nama tersebut belum dibuat, lalu akan dipindahkan dengan function rename. Kodenya
adalah sebagai berikut:
```c
void sortFun(char dirPath[])
{
    struct dirent *direntPoint;
    struct stat *fileStat;
    DIR *dirList = opendir(dirPath);
    char filePathTemp[100];
    threadCount = 0;

    if (!dirList)
    {
        // dirList empty error
        perror("dirList empty\n");
        exit(EXIT_FAILURE);
    }

    // Reads the folder
    while ((direntPoint = readdir(dirList)) != NULL)
    {
        if (strcmp("..", direntPoint->d_name) != 0 || strcmp(".", direntPoint->d_name) != 0)
        {
            // The ".." and "." will be filtered in this if statement
            sprintf(filePathTemp, "%s/%s", dirPath, direntPoint->d_name);
            // printf("DEBUG:: THIS IS filePathTemp: %s\n", filePathTemp);
            // if (stat(filePathTemp, fileStat) == 0 && S_ISREG(fileStat->st_mode))
            if ( access( filePathTemp, F_OK) == 0)
            {
                // Directory ada
                // moveFile(filePathTemp);
                pthread_t thread;
                pthread_create(&thread, NULL, &moveFile, (void *)filePathTemp);
                pthread_join(thread, NULL);
                // threadCount++;
            }
        }
    }

    // for (; threadCount > 0; threadCount--)
    // {
    //     // Joins all the threads
    //     pthread_join(tid[threadCount], NULL);
    // }

    closedir(dirList);
}
```
```c
void *moveFile(void *filePath)
{
    char *filePathInFunc = (char *)filePath;
    // printf("DEBUG:: This is filePath in moveFile func: %s\n", filePathInFunc);
    struct stat *sb;

    // char fileName[300] = strrchr(filePathInFunc, '/');
    char *fileName = strrchr(filePathInFunc, '/');
    fileName++;

    // printf("DEBUG:: This is fileName: %s\n", fileName);

    char categoryName[100], newDir[1000], fullNewDir[1000];
    char *extensionName;

    // printf("DEBUG:: filename[0] %c\n", fileName[0]);
    if (fileName[0] == '.')
    {
        // Hidden files
        sprintf(categoryName, "Hidden");
        // printf("DEBUG:: File dengan hidden files jalan\n");
    }

    else if ((extensionName = strstr(filePathInFunc, ".")) != NULL)
    {
        // File dengan ekstensi biasa
        extensionName++;
        char extensionTemp[100];
        strcpy(extensionTemp, extensionName);
        for(int i = 0; i < strlen(extensionTemp); i++)
        {
            extensionTemp[i] = tolower(extensionTemp[i]);
        }
        sprintf(categoryName, "%s", extensionTemp);
        // printf("DEBUG:: This is extensionName %s\n", extensionName);
    }

    else
    {
        // File dengan ekstensi yang tidak diketahui
        sprintf(categoryName, "Unknown");
        // printf("DEBUG:: File dengan ekstensi yang tidak diketahui ran\n");
    }

    // printf("DEBUG:: This is categoryName: %s\n", categoryName);

    // Membuat direktori baru
    sprintf(newDir, "%s/%s", dirPathWorking, categoryName);
    if (stat(newDir, sb) == 0 && S_ISDIR(sb->st_mode))
    {
        // Directory sudah ada
        // printf("dir udah ada\n");
    }
    else
    {
        // Directory belum ada
        mkdir(newDir, 0777);
    }

    // Memindahkan file
    sprintf(fullNewDir, "%s/%s", newDir, fileName);
    rename(filePathInFunc, fullNewDir);
}
```

d. e.
Untuk mengerjakan soal ini, dibutuhkan file client.c dan server.c Bagian pembuatan
dan koneksi soket sama dengan contoh yang tertera dalam github. Setelah itu, server
akan menjalankan function fileDownload yang akan menerima data dari client dan
melakukan fwrite ke hartakarun.zip. Penerimaan data menggunakan recv. 

Untuk client, menggunakan function fileSend yang akan menggunakan function send
untuk mengirim file tersebut. Kodenya adalah sebagai berikut:
```c
void fileSend(int sock, char fileName[])
{
    char buffer[1024] = {0};
    FILE *fptr = fopen(fileName, "rb");

    if(fptr == NULL)
    {
        perror("Error opening file\n");
        exit(EXIT_FAILURE);
    }

    bzero(buffer, 1024);

    size_t fileSize;
    while (true)
    {
        fileSize = fread(buffer, 1, 1024, fptr);
        printf("This is buffer: %s\n", buffer);

        if(fileSize > 0)
        {
            send(sock, buffer, 1024, 0);
        }

        else
        {
            break;
        }

        bzero(buffer, 1024);
    }
    
    printf("Selesai\n");
    fclose(fptr);
}
```
```c
void fileDownload(int socket)
{
    char buffer[1024] = {0};

    FILE *fptr = fopen(destPathGlobal, "ab");

    if(fptr == NULL)
    {
        perror("Error creating FILE pointer\n");
        exit(EXIT_FAILURE);
    }

    bzero(buffer, 1024);
    size_t fileSize = 0;
    while((fileSize = recv(socket, buffer, 1024, 0)) > 0)
    {
        if(fileSize == 0)
        {
            break;
        }
        size_t writeSize = fwrite(buffer, sizeof(char), fileSize, fptr);
        if(writeSize < fileSize)
        {
            perror("Error transferring files\n");
            exit(EXIT_FAILURE);
        }

        bzero(buffer, 1024);

    }

    fclose(fptr);

    printf("Berhasil\n");

}
```
