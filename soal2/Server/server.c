#include <stdio.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <pthread.h>
#include <ctype.h>
#define PORT 8080
#define SERVER_RETURN_SUCCESS "88"

#define SIZE 3000

typedef struct {
    int sck;
    char filename[1024];
} send_recv_file_info;


void *send_file_function(void *info){
    int n;
    FILE *fp;
    send_recv_file_info * fsinfo = (send_recv_file_info *) info;
    fp = fopen(fsinfo->filename, "r");
    char data[1024] = {0};
    
    while(fgets(data, 1024, fp) != NULL) {
        send(fsinfo->sck, data, sizeof(data), 0);
        bzero(data, 1024);
    }
    strcpy(data, "EOF");
    send(fsinfo->sck, data, sizeof(data), 0);
    fclose(fp);
}

void *write_file_function(void *info){
    int n;
    FILE *fp;
    char buffer[1024] = {0};
    send_recv_file_info * fsinfo = (send_recv_file_info *) info;
    fp = fopen(fsinfo->filename, "w");
    while (1) {
        recv(fsinfo->sck, buffer, 1024, 0);
        if (!strcmp(buffer, "EOF")){
            break;
        }
        fprintf(fp, "%s", buffer);    
        bzero(buffer, 1024);
    }
    fclose(fp);
}

void write_file(int sockfd, char *filename){
    send_recv_file_info info;
    info.sck = sockfd;
    strcpy(info.filename, filename);
    pthread_t thread_server;
    pthread_create(&thread_server, NULL, &write_file_function, &info);
    pthread_join(thread_server, NULL);
}

void send_file(const char *filename, int sockfd){
    send_recv_file_info info;
    info.sck = sockfd;
    strcpy(info.filename, filename);
    pthread_t thread_server;
    pthread_create(&thread_server, NULL, &send_file_function, &info);
    pthread_join(thread_server, NULL);
}

int isStrong(const char *password) {
    const char *p = password;
    char c;
    int nupper = 0;
    int nlower = 0;
    int ndigit = 0;
    while (*p) {
        c = *p++;
        if (isupper(c)) ++nupper;
        else if (islower(c)) ++nlower;
        else if (isdigit(c)) ++ndigit;
        else continue; // space character
   }
   return nupper && nlower && ndigit && (strlen(password) >= 6);
}

char** str_split(char* a_str, const char a_delim)
{
    char** result = 0;
    size_t count = 0;
    char* tmp = a_str;
    char* last_comma = 0;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = 0;

    while (*tmp)
    {
        if (a_delim == *tmp)
        {
            count++;
            last_comma = tmp;
        }
        tmp++;
    }

    count += last_comma < (a_str + strlen(a_str) - 1);
    count++;

    result = malloc(sizeof(char*) * count);

    if (result)
    {
        size_t idx  = 0;
        char* token = strtok(a_str, delim);

        while (token)
        {
            assert(idx < count);
            *(result + idx++) = strdup(token);
            token = strtok(0, delim);
        }
        assert(idx == count - 1);
        *(result + idx) = 0;
    }
    return result;
}

int isUniqueProblem(FILE *problems, char *judulProblem){
    char line[256];
    char **problem;
    while(fgets(line, sizeof(line), problems)){
        problem = str_split(line, '\t');
        if(!strcmp(judulProblem, problem[0])){
            return 0;
        }
    }
    return 1;
}

int isSame(FILE *file1, FILE *file2){
   char ch1 = getc(file1);
   char ch2 = getc(file2);
   int error = 0, pos = 0, line = 1;
   while (ch1 != EOF && ch2 != EOF){
      pos++;
      if (ch1 == '\n' && ch2 == '\n'){
         line++;
         pos = 0;
      }
      if (ch1 != ch2){
          return 0;
      }
      ch1 = getc(file1);
      ch2 = getc(file2);
   }
   return 1;
}


int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};

    char id[100];
    char pass[100];
    char idpass[100];
    char *success = "success";
    char *fail = "fail";
    char content[1024]={0};
    char *registered = "registered";
    char *login = "login";
    char *gagal = "gagal";
    char *down = "down";
    char *submit = "submit";

    char user[64];
    char isi[1024]={0};
    char string[50];
    int ret = 0;
    char file_down[1000]={0};

    FILE * tsv;
    tsv = fopen ("problems.tsv","a+");
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    valread = read(new_socket, buffer, 1024);

    // Register
    if (strcmp(buffer,"1")==0){
        send(new_socket, success, strlen(success), 0);
        memset(buffer, 0, sizeof(buffer));

        valread = read(new_socket, buffer, 1024);
        strcpy(idpass,buffer);
        strcat(idpass,":");

        FILE *fp = fopen ("akun.txt","a+");
        while (fscanf(fp,"%s", string) == 1){        
            if(strstr(string,idpass)!=0) {
                printf("Akun sudah ada.\n");
                ret = 1;
            }
        }

        valread = read(new_socket, buffer, 1024);
        strcat(idpass,buffer);
        
        if (!isStrong(buffer)){
            printf("Password tidak kuat.\n");
            ret = 1;
        }

        if (ret){
            send(new_socket, gagal, strlen(gagal), 0);
        } else {
            strcat(content, idpass);
            strcat(content,"\n");
            if(fp){
                fputs(content,fp);
            }
            fclose (fp);

            send(new_socket, registered, strlen(registered), 0);
        }
    } 
    
    // Login
    else if (strcmp(buffer,"2")==0){
        send(new_socket, success, strlen(success), 0);
        memset(buffer, 0, sizeof(buffer));

        long length;
        FILE *f = fopen ("akun.txt", "rb");

        if (f){
            valread = read(new_socket, buffer, 1024);
            strcpy(id,buffer);
            strcpy(idpass,buffer);
            strcat(idpass,":");
            valread = read(new_socket, buffer, 1024);
            strcpy(pass,buffer);
            strcat(idpass,buffer);

            while (fscanf(f,"%s", string) == 1){        
                if(strstr(string, idpass)!=0) {
                    ret = 1;
                }
            }

            if (ret){
                send(new_socket , login , strlen(login), 0);
                memset(buffer,0,sizeof(buffer));
                
                valread = read(new_socket , buffer, 1024);

                // Add
                if (strcmp(buffer,"add")==0){
                    FILE *fp;
                    char judulProblem[64] = {0};
                    char descFile[64] = {0};
                    char inputFile[64] = {0};
                    char outputFile[64] = {0};
                    char tmp[512];
                    char response[512];

                    fp = fopen("problems.tsv", "a+");
                    recv(new_socket, judulProblem, 64, 0);
                    printf("judul problem: %s\n", judulProblem);
                    if(isUniqueProblem(fp, judulProblem)){
                        strcpy(response, "able");
                        send(new_socket, response, strlen(response), 0);
                        mkdir(judulProblem, 0777);
                        fprintf(fp, "%s  \t %s\n", judulProblem, id);
                        recv(new_socket, descFile, 64, 0);
                        strcpy(tmp, judulProblem);
                        strcat(tmp, "/");
                        strcat(tmp, descFile);
                        printf("desc: %s\n", tmp);
                        write_file(new_socket, tmp);
                        printf("Data written to file\n");
                        recv(new_socket, inputFile, 64, 0);
                        strcpy(tmp, judulProblem);
                        strcat(tmp, "/");
                        strcat(tmp, inputFile);
                        printf("input: %s\n", tmp);
                        write_file(new_socket, tmp);
                        printf("Data written to file\n");
                        recv(new_socket, outputFile, 64, 0);
                        strcpy(tmp, judulProblem);
                        strcat(tmp, "/");
                        strcat(tmp, outputFile);
                        printf("output: %s\n", tmp);
                        write_file(new_socket, tmp);
                        printf("Data written to file\n");
                        strcpy(response, "Problem berhasil ditambahkan\n");
                        send(new_socket, response, strlen(response), 0);
                    } else{
                        strcpy(response, "unable");
                        send(new_socket, response, strlen(response), 0);
                    }
                    fclose(fp);

                }

                // See
                else if (strcmp(buffer,"see")==0){
                    char *buf = 0;
                    char content[1000]={0};
                    long length;
                    FILE * f = fopen ("problems.tsv", "rb");

                    if (f){
                        fseek (f, 0, SEEK_END);
                        length = ftell (f);
                        fseek (f, 0, SEEK_SET);
                        buf = malloc (length);
                        if (buf){
                            fread (buf, 1, length, f);
                        }
                        fclose (f);
                    }

                    if (buf){
                        strcpy(content,buf);
                        send(new_socket , content , strlen(content) , 0 );
                    }
                }

                // Download
                else if (strcmp(buffer,"down")==0){
                    char tmp[1024];
                    valread = read(new_socket, buffer, 1024);
                    strcpy(tmp, buffer);
                    strcat(tmp, "/");
                    strcat(tmp, "description.txt");
                    send_file(tmp, new_socket);
                    strcpy(tmp, buffer);
                    strcat(tmp, "/");
                    strcat(tmp, "input.txt");
                    send_file(tmp, new_socket);
                }
                
                // Submit
                else if (strcmp(buffer,"submit")==0){
                    FILE *answer, *key;
                    char buffer[1024] = {0};
                    char tmp[1024];
                    valread = read(new_socket, buffer, 1024);
                    strcpy(tmp, buffer);
                    strcat(tmp, "/output.txt");
                    write_file(new_socket, "tmp.txt");
                    answer = fopen("tmp.txt", "r");
                    key = fopen(tmp, "r");
                    if(isSame(answer, key)){
                        strcpy(tmp, "AC\n");
                    } else{
                        strcpy(tmp, "WA\n");
                    }
                    send(new_socket, tmp, strlen(tmp), 0);
                    fclose(answer);
                    fclose(key);
                    remove("tmp.txt");
                    printf("Done\n");
                }

                else{
                    printf("Command tidak ditemukan.\n");
                }
                

            } else {
                printf("Account not found.\n");
                send(new_socket, gagal, strlen(gagal), 0);
            }
        } else {
            printf("File not found.\n");
            send(new_socket, gagal, strlen(gagal), 0);
        }
    } 
    return 0;
}