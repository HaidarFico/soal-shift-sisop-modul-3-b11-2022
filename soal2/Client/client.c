#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#define PORT 8080
#define SERVER_RETURN_SUCCESS "88"

#define SIZE 3000

char response[2048];

typedef struct {
    int sck;
    char filename[1024];
} send_recv_file_info;

void *send_file_function(void *info){
    FILE *fp;
    send_recv_file_info * fsinfo = (send_recv_file_info *) info;
    fp = fopen(fsinfo->filename, "r");
    char data[1024] = {0};
    
    while(fgets(data, 1024, fp) != NULL) {
        send(fsinfo->sck, data, sizeof(data), 0);
        bzero(data, 1024);
    }
    strcpy(data, "EOF");
    send(fsinfo->sck, data, sizeof(data), 0);
    fclose(fp);
}

void *write_file_function(void *info){
    FILE *fp;
    char buffer[1024];
    send_recv_file_info * fsinfo = (send_recv_file_info *) info;
    fp = fopen(fsinfo->filename, "w");
    while (1) {
        recv(fsinfo->sck, buffer, 1024, 0);
        if (!strcmp(buffer, "EOF"))
        {
            break;
        }
        fprintf(fp, "%s", buffer);
        bzero(buffer, 1024);
    }
    fclose(fp);
}

void *read_file_function(void *sockfd){
    char buffer[1024];
    int * socket_server = (int *) sockfd;
    while (1) {
        recv(*socket_server, buffer, 1024, 0);
        if (!strcmp(buffer, "EOF"))
        {
            break;
        }
        printf("%s", buffer);
        bzero(buffer, 1024);
    }
}

void read_file(int sockfd){
    pthread_t thread_client;
    pthread_create(&thread_client, NULL, &read_file_function, &sockfd);
    pthread_join(thread_client, NULL);
}

void write_file(int sockfd, char *filename){
    send_recv_file_info info;
    info.sck = sockfd;
    strcpy(info.filename, filename);
    pthread_t thread_client;
    pthread_create(&thread_client, NULL, &write_file_function, &info);
    pthread_join(thread_client, NULL);
}

void send_file(const char *filename, int sockfd){
    send_recv_file_info info;
    info.sck = sockfd;
    strcpy(info.filename, filename);
    pthread_t thread_client;
    pthread_create(&thread_client, NULL, &send_file_function, &info);
    pthread_join(thread_client, NULL);
}

int isSame(FILE *file1, FILE *file2){
   char ch1 = getc(file1);
   char ch2 = getc(file2);
   int error = 0, pos = 0, line = 1;
   while (ch1 != EOF && ch2 != EOF){
      pos++;
      if (ch1 == '\n' && ch2 == '\n'){
         line++;
         pos = 0;
      }
      if (ch1 != ch2){
          return 0;
      }
      ch1 = getc(file1);
      ch2 = getc(file2);
   }
   return 1;
}

int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[1024] = {0};
    FILE* fp;

    int pil;
    char pilBuff[50], idBuff[10], passBuff[10];
    char *regist = "1";
    char *login = "2"; 
    char *add = "add";
    char *see = "see";
    char *down = "down";
    char *submit = "submit";

    char id[100];
    char pass[100];
    char idc[3]={0};
    char kirim[200]={0};
    char isi[1000]={0};
    char download[200]={0};
    char judul[100];
    char path[100];
    char judulProblem[64] = {0};
    char descFile[64] = {0};
    char inputFile[64] = {0};
    char outputFile[64] = {0};

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return -1;
    }

    printf("Menu:\n1. Register\n2. Login\n\n");
    scanf("%d",&pil);
    printf("\n");
    if (pil==1){
        send(sock, regist, strlen(regist), 0);
        printf("Mohon masukkan baru ID dan sebuah password.\n");
    }
    else if (pil==2){
        send(sock, login, strlen(login), 0);
        printf("Mohon masukkan ID dan password yang sudah ter-register.\n");
    }
    valread = read(sock , buffer, 1024);

    if (strcmp(buffer,"success")==0){
        memset(buffer,0,sizeof(buffer));

        printf("Masukkan ID: ");
        scanf("%s",id);
        send(sock, id, strlen(id), 0);

        printf("Masukkan password: ");
        scanf("%s",pass);
        send(sock, pass, strlen(pass), 0);
        printf("\n");
        
        valread = read(sock, buffer, 1024);
        if (strcmp(buffer,"login")==0){
            printf("Login sukses!\n");

            char cmd[100]={0};
            printf("Masukkan command: ");
            scanf("%s",cmd);

            // Add
            if(strcmp(cmd,"add")==0){
                send(sock, add, strlen(add), 0 );

                printf("Add Problem\n");
                printf("Judul Problem: ");
                scanf(" %s", judulProblem);
                send(sock, judulProblem, strlen(judulProblem), 0);
                memset(response, 0, 2048);
                recv(sock, response, 2048, 0);
                if(!strcmp(response, "able")){
                    printf("Filepath description.txt: ");
                    scanf(" %s", descFile);
                    send(sock, descFile, strlen(descFile), 0);
                    send_file(descFile, sock);
                    printf("Filepath input.txt: ");
                    scanf(" %s", inputFile);
                    send(sock, inputFile, strlen(inputFile), 0);
                    send_file(inputFile, sock);
                    printf("Filepath output.txt: ");
                    scanf(" %s", outputFile);
                    send(sock, outputFile, strlen(outputFile), 0);
                    send_file(outputFile, sock);
                    memset(response, 0, 2048);
                    recv(sock, response, 2048, 0);
                    printf("%s", response);
                }
                if(!strcmp(response, "unable")){
                    printf("Problem dengan judul tersebut sudah ada, gunakan judul lain\n");
                }

            }

            // See
            else if (strcmp(cmd,"see")==0){
                send(sock , see , strlen(see) , 0 );
                memset(buffer,0,sizeof(buffer));
                valread = read(sock, buffer, 1024);
                strcat(isi,buffer);

                int i;
                while(isi[i]!='\0'){
                    if(isi[i]=='\t'){
                        isi[i-1]='b';
                        isi[i]='y';
                    }
                    i++;
                }

                printf("\n%s",isi);
            }

            // Download
            else if (strcmp(cmd,"download")==0){
                char tmp[1024];
    
                send(sock, download, strlen(download), 0);
                sleep(1);
                strcpy(tmp, judul);
                send(sock, tmp, strlen(tmp), 0);
                mkdir(judul, 0777);
                strcpy(tmp, judul);
                strcat(tmp, "/description.txt");
                write_file(sock, tmp);
                strcpy(tmp, judul);
                strcat(tmp, "/input.txt");
                write_file(sock, tmp);
                printf("Problem downloaded\n");
            }
            

            // Submit
            else if (strcmp(cmd,"submit")==0){
                FILE *answer, *key;
                char tmp[1024];
                answer = fopen("answer.txt", "r");
                key = fopen("key.txt", "r");
                if(isSame(answer, key)){
                    printf("AC\n");
                } else{
                    printf("WA\n");
                }
                fclose(answer);
                fclose(key);
                remove("tmp.txt");
                printf("Done\n");
            }

            else{
                printf("Command tidak ditemukan.\n");
            }  
            

        } else if (strcmp(buffer,"registered")==0){
            printf("Account ter-register!\n");

        } else{
            printf("Gagal.\n");
        }
    } else {
        printf("Gagal.\n");

    }

    return 0;
}